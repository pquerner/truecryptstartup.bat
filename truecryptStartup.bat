@echo off

set container=dropbox.tc
set driveLetter=T
set keyDir=sshkeys
set keyName=id_rsa

start "" "%ProgramFiles%\TrueCrypt\TrueCrypt.exe" /q /v "%UserProfile%\Dropbox\%container%" /l %driveLetter% /a
tasklist /FI "IMAGENAME eq TrueCrypt.exe" 2>NUL | find /I /N "TrueCrypt.exe">NUL
if "%ERRORLEVEL%"=="0" (
    goto :checkagain
    :checkagain
        if NOT EXIST "%driveLetter%:\%keyDir%\%keyName%" ( 
            goto :checkagain
        )
        if EXIST "%driveLetter%:\%keyDir%\%keyName%" ( 
            start "" "%driveLetter%:\%keyDir%\PAGEANT.EXE" "%driveLetter%:\%keyDir%\%keyName%"
        )
    )
Exit